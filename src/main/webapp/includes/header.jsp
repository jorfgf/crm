<%-- 
    Document   : index
    Created on : 06-06-2021, 14:32:08
    Author     : joseera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title> CRM | JAVA </title>
  </head>
  <body>
      
      <header>
          <!--Navbar--->
          <nav class="navbar navbar-dark bg-dark">
            <div class="container">
              <a class="navbar-brand" href="#">
                  <h3>CRM | JAVA</h3>
              </a>
            </div>
            
            <div class="dropdown" style="margin: 0 20px">
                <button class="btn btn-link dropdown-toggle" style="text-decoration: none!important" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    <span style="color:white; font-size: 1.2rem ">Nombre Usuario</span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                  <li><a class="dropdown-item" href="#">Perfil</a></li>
                  <li><a class="dropdown-item" href="#">Cerrar Sesion</a></li>
                </ul>
            </div>
          </nav>
          
     
      </header>
     
      <div class="row">
          
          <div class="col-sm-2" style="background: #212529f0!important;color: white;height:100vh">
            <aside>
                <!--SideBar-->
                <div class="wrapper fixed-left">
                    <nav id="sidebar">
                        <div class="sidebar-header">
                            <h3 style="padding: 15px;">Funciones</h3>
                        </div>

                        <ul class="list-unstyled components" style="background: #0d6efd;padding: 8px;border-radius: 10px;margin: 10px;">
                            <li>
                                <a href="index.jsp" style="color: #fff;text-decoration: none;padding: 10px;">Clientes</a>
                            </li>
                        </ul>
                    </nav>
                </div>

              </aside>
          </div>
          <div class="col-sm-10">