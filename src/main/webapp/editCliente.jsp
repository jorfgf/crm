
<%@page import="Entity.Cliente"%>
<%@page import="Bean.JavaBeanCliente"%>
<%@page import="java.util.ArrayList"%>

<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="includes/header.jsp"/>

<main>
     <div class="container p-5">
         
         <div id="header-main">
            <h3>Clientes</h3>
            <span>Usted esta modificando a un cliente.</span>
            
            <% String id = request.getParameter("id"); 
            out.print(id);   %>
         </div>
         
         
         <div id="contenedor-cliente" class="mt-3">
             
             <table class="table table-dark table-striped">
                <thead>
                   <tr>
                     <th scope="col">#</th>
                     <th scope="col">Nombre</th>
                     <th scope="col">Apellido</th>
                     <th scope="col">Region</th>
                     <th scope="col">Comuna</th>
                     <th scope="col">Direccion</th>
                     <th scope="col">Telefono</th>
                     <th scope="col"> Actualizar</th>
                   
                   </tr>
                 </thead>
                 <tbody>
                 <%
                    JavaBeanCliente clcurd = new JavaBeanCliente();
                    List<Cliente> listaClientes = clcurd.buscarUno(Integer.parseInt(id));

                    for (Cliente cliente : listaClientes) {%>
                    
                 <form action="ServletUpdateCliente" method="POST">
                   <tr>
                     <th scope="row"><%out.print(cliente.getId());%></th>
                     <input type="hidden" value="<%out.print(cliente.getId());%>" name="id">
                     <td><input type="text" value="<%out.print(cliente.getName());%>" class="form-control" name="name"></td>
                     <td><input type="text" value="<%out.print(cliente.getLastname());%>" class="form-control" name="lastname"> </td>
                     <td><input type="text" value="<%out.print(cliente.getRegion());%>" class="form-control" name="region"> </td>
                     <td><input type="text" value="<%out.print(cliente.getComuna());%>" class="form-control" name="comuna"> </td>
                     <td><input type="text" value="<%out.print(cliente.getDirection());%>" class="form-control" name="direction"> </td>
                     <td><input type="text" value="<%out.print(cliente.getPhone());%>" class="form-control" name="phone"> </td>
                     <td>
                         <button class="btn btn-primary" name="btn">
                             Actualizar
                         </button>
                     </td>

                   </tr>
                 </form>
                   <% } %>
                 
                 </tbody>
            </table>
         </div>
         


     </div>


</main>

<jsp:include page="includes/footer.jsp"/>