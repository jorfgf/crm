
<%@page import="Bean.JavaBeanCliente"%>
<%@page import="Entity.Cliente"%>
<%@page import="java.util.ArrayList"%>

<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="includes/header.jsp"/>

<main>
     <div class="container p-5">
         
            <%
               String mensaje = (String) request.getAttribute("mensaje");
               String type = (String) request.getAttribute("type");
               if (mensaje != null) {
                   out.println("<div class='alert alert-"+type+"'>" + mensaje + "</div>");
               }
           %>
                    
         <div id="header-main">
            <h3>Clientes</h3>
            <span>A continuacion podra hacer las modificaciones correspondientes 
         al modulo clientes, como crear, editar y eliminar.</span>
         </div>
         
         
         <div id="contenedor-cliente" class="mt-3">
             <div style="display:flex;justify-content:flex-end">
                <button class="btn btn-secondary mb-2 ml-auto" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar uno nuevo</button>
             </div>
             
             <table class="table table-dark table-striped">
                <thead>
                   <tr>
                     <th scope="col">#</th>
                     <th scope="col">Nombre</th>
                     <th scope="col">Apellido</th>
                     <th scope="col">Region</th>
                     <th scope="col">Comuna</th>
                     <th scope="col">Direccion</th>
                     <th scope="col">Telefono</th>
                     <th scope="col"> Editar </th>
                     <th scope="col"> Eliminar </th>
                   </tr>
                 </thead>
                 <tbody>
                 <%
                    JavaBeanCliente clcurd = new JavaBeanCliente();
                    List<Cliente> listaClientes = clcurd.buscar();

                    for (Cliente cliente : listaClientes) {%>
                   <tr>
                     <th scope="row"><%out.print(cliente.getId());%></th>
                     <td><%out.print(cliente.getName());%></td>
                     <td><%out.print(cliente.getLastname());%></td>
                     <td><%out.print(cliente.getRegion());%></td>
                     <td><%out.print(cliente.getComuna());%></td>
                     <td><%out.print(cliente.getDirection());%></td>
                     <td><%out.print(cliente.getPhone());%></td>
                     <td>
                         <a href="editCliente.jsp?id=<%out.print(cliente.getId());%>" class="btn btn-primary">
                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                              </svg>
                         </a>
                         
                     </td>
                     <td>
                         <form action="ServletEliminarCliente" method="POST">
                             <input type="hidden" name="id_cliente" value="<%out.print(cliente.getId());%>">
                              <button type="submit" class="btn btn-danger" onclick="confirm('Desea Eliminar a: <%out.print(cliente.getName());%> <%out.print(cliente.getLastname());%>')">
                                 <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                 </svg>
                             </button>
                             
                         </form>
                         
                     </td>
                   </tr>
                   
                   <% } %>
                 
                 </tbody>
            </table>
         </div>
         
         <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Aregar un nuevo cliente</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                  <form action="ServletClientes" method="POST">
                      <div class="row">
                          <div class="col-sm-6">
                              <div class="from-group">
                                  <lable>Nombre</lable>
                                  <input type="text" name="name" class="form-control">
                              </div>
                              <div class="from-group">
                                  <lable>Telefono</lable>
                                  <input type="text" name="phone" class="form-control">
                              </div>
                              <div class="from-group">
                                  <lable>Comuna</lable>
                                  <input type="text" name="comuna" class="form-control">
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="from-group">
                                  <lable>Apellido</lable>
                                  <input type="text" name="lastname" class="form-control">
                              </div>
                              <div class="from-group">
                                  <lable>Region</lable>
                                  <input type="text" name="region" class="form-control">
                              </div>
                              <div class="from-group">
                                  <lable>Direccion</lable>
                                  <input type="text" name="direction" class="form-control">
                              </div>
                          </div>
                      </div>
                     
              </div>
              <div class="modal-footer">
                  <input type="submit" class="btn btn-outline-success mt-2" value="Guardar">
              </div>
                 </form>
            </div>
          </div>
        </div>


     </div>


</main>

<jsp:include page="includes/footer.jsp"/>