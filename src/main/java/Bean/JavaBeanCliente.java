/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import DAO.ClienteJpaController;
import DAO.exceptions.NonexistentEntityException;
import Entity.Cliente;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author joseera
 */
public class JavaBeanCliente {
    
    private EntityManagerFactory emf;
    private ClienteJpaController clienteControl;

    public JavaBeanCliente() {
        emf = Persistence.createEntityManagerFactory("prod");
        clienteControl = new ClienteJpaController(emf);
    }
    
    public void guardar(Cliente c){
        clienteControl.create(c);
    }
    
    public List<Cliente> buscar(){
        return clienteControl.findClienteEntities();
    }
    
    public List<Cliente> buscarUno(int id){
        return (List<Cliente>) clienteControl.findCliente(id);
    }
     
    public void actualizar(Cliente c) throws Exception{
       clienteControl.edit(c);
    }
    
   public void borrar(int id) throws NonexistentEntityException{
       clienteControl.destroy(id);
   }
   
}
