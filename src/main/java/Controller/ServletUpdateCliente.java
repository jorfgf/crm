/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;


import Bean.JavaBeanCliente;
import Entity.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author joseera
 */
public class ServletUpdateCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
                if (request.getParameter("btn") != null) {
            String id = request.getParameter("id");
            String name = request.getParameter("name");
            String lastname = request.getParameter("lastname");
            String phone = request.getParameter("phone");
            String region = request.getParameter("region");
            String direction = request.getParameter("direction");
            String comuna = request.getParameter("comuna");

            try {
                Cliente cliente = new Cliente();
                cliente.setName(name);
                cliente.setId(Integer.parseInt(id));
                cliente.setLastname(lastname);
                cliente.setPhone(phone);
                cliente.setComuna(comuna);
                cliente.setDirection(direction);
                cliente.setRegion(region);

                JavaBeanCliente ccrud = new JavaBeanCliente();
                ccrud.actualizar(cliente);

                request.setAttribute("mensaje", "Se han actualizado sus datos correctamente");
                request.setAttribute("type", "success");
                request.getRequestDispatcher("index.jsp").forward(request, response);

            } catch (Exception e) {
                request.setAttribute("mensaje", "Algo salio mal");
                request.setAttribute("type", "warning");
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN,"No puede ingresar a esta pagina");
        }

    

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
