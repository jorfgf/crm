/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;


import Bean.JavaBeanCliente;
import Entity.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author joseera
 */
public class ServletClientes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        response.setContentType("text/html;charset=UTF-8");
               
        String name = request.getParameter("name");
        String lastname = request.getParameter("lastname");
        String phone = request.getParameter("phone");
        String region = request.getParameter("region");
        String comuna = request.getParameter("comuna");
        String direction = request.getParameter("direction");
        String msg2;
         

        try {
            Cliente cliente = new Cliente();
            cliente.setName(name);
            cliente.setLastname(lastname);
            cliente.setPhone(phone);
            cliente.setRegion(region);
            cliente.setComuna(comuna);
            cliente.setDirection(direction);

            JavaBeanCliente ccrud = new JavaBeanCliente();
            ccrud.guardar(cliente);
            
            msg2 = "Se ha registrado satisfactoriamente";
            request.setAttribute("mensaje", msg2);
            request.setAttribute("type", "success");
            request.getRequestDispatcher("index.jsp").forward(request, response);


        } catch (IOException | NumberFormatException | ServletException ex) {
            request.setAttribute("mensaje", "Algo Salio Mal");
            request.setAttribute("type", "warning");
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
